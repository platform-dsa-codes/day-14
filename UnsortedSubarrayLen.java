import java.util.*;
import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            int n = Integer.parseInt(br.readLine().trim());
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }

            int ans[] = new Solve().printUnsorted(arr, n);
            System.out.println(ans[0] + " " + ans[1]);
        }
    }
}


class Solve {
    int[] printUnsorted(int[] arr, int n) {
        int start = 0, end = n - 1;
        
        while (start < n - 1 && arr[start] <= arr[start + 1]) {
            start++;
        }
        
        if (start == n - 1) {
            return new int[]{0, 0};
        }
        
        while (end > 0 && arr[end] >= arr[end - 1]) {
            end--;
        }
        
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        for (int i = start; i <= end; i++) {
            min = Math.min(min, arr[i]);
            max = Math.max(max, arr[i]);
        }
        
        while (start > 0 && arr[start - 1] > min) {
            start--;
        }
        while (end < n - 1 && arr[end + 1] < max) {
            end++;
        }
        
        return new int[]{start, end};
    }
}
