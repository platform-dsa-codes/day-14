import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	public static void main(String[] args) throws IOException
	{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine().trim());
        while(t-->0)
        {
            StringTokenizer stt = new StringTokenizer(br.readLine());
            
            int n = Integer.parseInt(stt.nextToken());
            int m = Integer.parseInt(stt.nextToken());
            // int n = Integer.parseInt(br.readLine().trim());
            int a[] = new int[n];
            String inputLine[] = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(inputLine[i]);
            }
            
            Solution obj = new Solution();
            System.out.println(obj.smallestSubWithSum(a, n, m));
        }
	}
}


class Solution {
    public static int smallestSubWithSum(int a[], int n, int x) {
        int minLength = n + 1;
        int currentSum = 0;
        int start = 0;
        int end = 0;

        while (end < n) {
            while (currentSum <= x && end < n) {
                if (currentSum <= 0 && x > 0) {
                    start = end;
                    currentSum = 0;
                }
                currentSum += a[end];
                end++;
            }

            while (currentSum > x && start < n) {
                if (end - start < minLength) {
                    minLength = end - start;
                }
                currentSum -= a[start];
                start++;
            }
        }

        if (minLength == n + 1) {
            return 0;
        } else {
            return minLength;
        }
    }
}
