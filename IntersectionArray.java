import java.util.*;

public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> resultSet = new HashSet<>();

        for (int num : nums1) {
            set1.add(num);
        }

        for (int num : nums2) {
            if (set1.contains(num)) {
                resultSet.add(num);
            }
        }

        int[] result = new int[resultSet.size()];
        int index = 0;
        for (int num : resultSet) {
            result[index++] = num;
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the size of the first array (nums1):");
        int size1 = scanner.nextInt();
        int[] nums1 = new int[size1];
        System.out.println("Enter the elements of the first array (nums1):");
        for (int i = 0; i < size1; i++) {
            nums1[i] = scanner.nextInt();
        }

        System.out.println("Enter the size of the second array (nums2):");
        int size2 = scanner.nextInt();
        int[] nums2 = new int[size2];
        System.out.println("Enter the elements of the second array (nums2):");
        for (int i = 0; i < size2; i++) {
            nums2[i] = scanner.nextInt();
        }

        Solution solution = new Solution();

        int[] intersection = solution.intersection(nums1, nums2);

        System.out.println("Intersection of nums1 and nums2:");
        for (int num : intersection) {
            System.out.print(num + " ");
        }
    }
}
